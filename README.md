# xraylib-numpy

Thin wrapper providing python bindings for the [xraylib library](https://github.com/tschoonj/xraylib/wiki) installable via pip.

Uses meson and mesonpep517.

## Changes

 - Rewritten meson build configuration from scratch
 - Removed google analytics
 - Removed bindings for deprecated functions
 - Some cleanup to allow building against xraylib-devel without the xraylib sources

## Installing

Requires `xraylib`, `xraylib-devel`, `swig` and C compiler.

Clone repository:
```
git clone git@gitlab.gwdg.de:leonmerten.lohse1/xraylib-numpy.git
cd xraylib-numpy
```

Install:
```
python3 -m pip install --user .
```

## Plans
 - Add vectorized bindings for the most important functions
